import unittest
from ..game import Game

class TestGame(unittest.TestCase):
    def test_init(self):
        game = Game("player1", "player2")
        self.assertEqual(game.active_player, None)
        self.assertEqual(game.players, ["player1", "player2"])
        self.assertEqual(game.board.size, 3)
        self.assertEqual(game.board.board, {(0, 0): "", (0, 1): "", (0, 2): "",
                                            (1, 0): "", (1, 1): "", (1, 2): "",
                                            (2, 0): "", (2, 1): "", (2, 2): ""})
        self.assertEqual(game.winner, None)
    
    # Test the call method with no active player set
    def test_call(self):
        game = Game("player1", "player2")
        self.assertEqual(game.active_player, None)
        game()
        self.assertNotEqual(game.active_player, None)

    def test_call_active_player(self):
        game = Game("player1", "player2")
        game("player1")
        self.assertEqual(game.active_player, "player1")

    def test_play(self):
        game = Game("player1", "player2")
        game("player1")
        self.assertEqual(game.active_player, "player1")
        game.play("player1", (0, 0))
        self.assertEqual(game.board[(0, 0)], "X")
        game.play("player2", (0, 1))
        self.assertEqual(game.board[(0, 1)], "O")

    def test_play_invalid_move(self):
        game = Game("player1", "player2")
        game("player1")
        self.assertEqual(game.active_player, "player1")
        game.play("player1", (0, 0))
        self.assertEqual(game.board[(0, 0)], "X")
        game.play("player2", (0, 1))
        self.assertEqual(game.board[(0, 1)], "O")
        with self.assertRaises(Exception):
            game.play("player1", (0, 0))
    
    def test_play_illegal_move(self):
        game = Game("player1", "player2")
        game("player1")
        self.assertEqual(game.active_player, "player1")
        with self.assertRaises(Exception):
            game.play("player1", (4, 4))

    def test_play_wrong_player(self):
        game = Game("player1", "player2")
        game("player1")
        self.assertEqual(game.active_player, "player1")
        with self.assertRaises(Exception):
            game.play("player2", (0, 0))

    def test_tie_game(self):
        game = Game("player1", "player2")
        game("player1")
        """
        X | X | O
        ----------
        O | X | X
        ----------
        X | O | O
        """
        game.play("player1", (0, 0))
        game.play("player2", (0, 2))
        game.play("player1", (0, 1))
        game.play("player2", (1, 0))
        game.play("player1", (1, 1))
        game.play("player2", (2, 2))
        game.play("player1", (1, 2))
        game.play("player2", (2, 1))
        game.play("player1", (2, 0))
        self.assertEqual(game.winner, "Draw")

    def test_winner_quick_game(self):
        game = Game("player1", "player2")
        game("player1")
        """
        X | X | O
        ----------
          | O |   
        ----------
        O |   | X
        """
        game.play("player1", (0, 0))
        game.play("player2", (0, 2))
        game.play("player1", (0, 1))
        game.play("player2", (1, 1))
        game.play("player1", (2, 2))
        game.play("player2", (2, 0))
        self.assertEqual(game.winner, "player2")

    def test_winner_full_game(self):
        game = Game("player1", "player2")
        game("player1")
        """
        X | X | O
        ----------
        X | O | X
        ----------
        X | O | O
        """
        game.play("player1", (0, 0))
        game.play("player2", (0, 2))
        game.play("player1", (0, 1))
        game.play("player2", (1, 1))
        game.play("player1", (1, 0))
        game.play("player2", (2, 2))
        game.play("player1", (1, 2))
        game.play("player2", (2, 1))
        game.play("player1", (2, 0))
        self.assertEqual(game.winner, "player1")

    def test_available_moves(self):
        game = Game("player1", "player2")
        game("player1")
        game.play("player1", (0, 0))
        game.play("player2", (0, 1))
        self.assertEqual(game.available_moves(), [(0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)])