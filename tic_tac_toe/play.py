from tic_tac_toe import PlayAdapter, Game

class Play(object):
    """
    A class used to play tic-tac-toe. 

    Accepts player names and prompts for player moves until the game is over.

    """

    def __init__(self, ui):
        """
        Initializes a new instance of the class.

        Parameters:
            self: The object itself.

        Returns:
            None
        """
        self.ui = ui
        self._active_game = None

    def start(self, adapter1: PlayAdapter, adapter2: PlayAdapter, training=False):
        """
        Starts a new game between two players using the provided PlayAdapter instances.

        Args:
            adapter1 (PlayAdapter): The PlayAdapter instance representing the first player.
            adapter2 (PlayAdapter): The PlayAdapter instance representing the second player.
            training (bool): Whether the game is for training purposes or not. Defaults to False.

        Raises:
            Exception: If a game is already in progress.

        Returns:
            None
        """
        
        if(self._active_game):
            raise("Game already in progress")
        
        p1 = adapter1.get_name()
        p2 = adapter2.get_name()

        self._active_game = Game(p1, p2)
        adapter1.set_game(self._active_game)
        adapter2.set_game(self._active_game)

        self._active_game()

        while(True):
            game = self._active_game
            player = game.active_player
            self._display()
            if(player == p1):
                move = adapter1.next_move()
            else:
                move = adapter2.next_move()

            # this indicates quitting
            if move == None:
                break

            try:
                game.play(player, move)
                if(game.winner):
                    self._display()
                    self._message(f"winner: {game.winner}")
                    if(training):
                        self._train()
                    # Let 1st player decide whether to continue
                    if(adapter1.new_game()):
                        self._active_game = Game(p1, p2)
                        
                        adapter1.set_game(self._active_game)
                        adapter2.set_game(self._active_game)

                        # initialize the game
                        self._active_game()
                    else:
                        break
            except Exception as e:
                self._message(e)

    def _train(self):
        pass

    def _message(self, msg):
        self.ui.message(msg)
    
    def _display(self):
        self.ui.display(self._active_game)