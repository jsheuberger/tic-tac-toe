from typing import List
from .policy import Policy
import random

class RandomPolicy():

    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size

    def sample(self, inputs: List[float]):
        # only use legal actions
        legal_actions = [i for i in range(self.action_size) if inputs[i] == 0]
        r_index = random.randint(0, len(legal_actions))
        return legal_actions[r_index]