from typing import Any, Tuple
from .play_adapter import PlayAdapter

class UserAdapter(PlayAdapter):

    def __init__(self, ui):
        self.ui = ui
        self.name = None

    def get_name(self) -> str:
        if(self.name == None):
            self.name = self.ui.prompt("Enter your name: ")
        return self.name
    
    def next_move(self) -> Tuple[int, int]:
        while True:
            move = self.ui.prompt("Enter your next move: ")
            if(move == "quit" or move == "q"):
                return None
            try:
                move = tuple(int(x) for x in move.split(","))
                if(len(move) == 2):
                    return move
                else:
                    self.ui.message("Invalid move: (q)uit to exit")
            except:
                self.ui.message("Invalid move: (q)uit to exit")


    def new_game(self) -> bool:
        while True:
            new = self.ui.prompt("start a new game? Y/n: ")
            if(new == "Y" or new == "y"):
                return True
            elif(new == "N" or new == "n"):
                return False
            else:
                self.ui.message("Invalid input.")

    def set_game(self, game):
        pass
