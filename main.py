import json
from tic_tac_toe import Play, UI, UserAdapter, Replay, load_from_json

def main(mode="play", agent_file="", target_file="", games=100, batch_size=1, replay_file="", delay=1000, display=False, self_play=False):
    if mode == "play":
        # Start a normal game
        ui = UI()
        user1 = UserAdapter(ui)
        user2 = UserAdapter(ui)

        # Start the play session
        Play(ui).start(user1, user2, training=False)

    elif mode == "play-ai":
        from tic_tac_toe import AgentAdapter
        from agent.agent import new_agent, load_agent, save_agent

        # Load the agent, and start a game
        if(agent_file != ""): 
            agent = load_agent(agent_file, 'agent-3')
        else:
            agent = new_agent("tttbot", policy_type="random")
        
        ui = UI()
        user_adapter = UserAdapter(ui)
        agent_adapter = AgentAdapter(agent, None)
        
        Play(ui).start(user_adapter, agent_adapter, training=False)

        # save the records if the filename is set
        if replay_file != "":
            records = {"games": [[("player1", (0, 0)), ("player2", (2, 2))], 
                                 [("player1", (2, 2)), ("player2", (0, 0))]]}
            with open(replay_file, "w") as f:
                json.dump(records, f)
    elif mode == "train":
        from agent.policy_gradient_model import PolicyGradientModel
        from agent.policy_gradient_training import PolicyGradientTraining
        from agent.rewards import Rewards

        # Try to load the agent if a file is given, else create a new one
        model = PolicyGradientModel(state_size=9, action_size=9, hidden_size=(32,16))

        if(agent_file != ""):
            import os
            if os.path.exists(agent_file):
                model.load(agent_file)
            else:
                raise Exception("Agent file not found: " + agent_file)

        rewards = Rewards()
        training = PolicyGradientTraining(reward_object=rewards, learning_rate=0.1, gamma=0.98)

        training.train(model, episode_count=games, batch_size=batch_size, display=display, self_play=self_play)
        
        # Save the agents
        if(target_file != ""):
            model.save(target_file)
                
    elif mode == "replay":
        ui = UI()
        replay = Replay(ui)
        # Load the records from a json file
        with open(replay_file, "r") as f:
            loaded = json.load(f)
        
        # Replay the records
        players, games = load_from_json(loaded)
        for moves in games:
            replay.replay(moves, players, delay)

if(__name__ == '__main__'):
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", help="The mode to run the program in: play, play-ai, train, replay", type=str, default="play")
    parser.add_argument("--agent-file", help="The file to load the agent from", type=str, default="")
    parser.add_argument("--target-file", help="The file to save the agent to", type=str, default="")
    parser.add_argument("--replay-file", help="The file to load/save the replay records from/to", type=str, default="")
    parser.add_argument("--games", help="The number of games to train for", type=int, default=100)
    parser.add_argument("--batchsize", help="The number of games to train in a batch", type=int, default=10)
    parser.add_argument("--delay", help="Delay times in display of replay moves, 0 for manual", type=int, default=1000)
    parser.add_argument("--display", help="Display the replay moves", default=False, action="store_true")
    parser.add_argument("--self-play", help="Play against random moves", default=False, action="store_true")
    args = parser.parse_args()

    main(mode=args.mode, agent_file=args.agent_file, target_file=args.target_file, games=args.games, batch_size=args.batchsize, delay=args.delay, 
         display=args.display, replay_file=args.replay_file, self_play=args.self_play)