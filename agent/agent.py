from typing import List
from .policy import Policy
from .policy_gradient_model import PolicyGradientModel
from .random_policy import RandomPolicy

class Agent(object):

    def __init__(self, name, policy: Policy):
        self.name = name
        self.policy = policy

    def predict(self, state: List[float]) -> int:
        probs = self.policy.call(state)
        return self.policy.sample(probs)
    
    def train(self, episodes, rewards):
        pass

def load_agent(file, name):
    agent = Agent(name, PolicyGradientModel(state_size=9, action_size=9, hidden_size=(64,32)))
    agent.policy.load(file)
    return agent

def save_agent(agent):
    pass

def new_agent(name, policy_type="random"):
    if(policy_type == "pg"):
        policy = PolicyGradientModel(state_size=9, action_size=9, hidden_size=18)
    elif(policy_type == "random"):
        policy = RandomPolicy(state_size=9, action_size=9)
    else:
        raise("Unknown policy type: " + policy_type)
    return Agent(name, policy)