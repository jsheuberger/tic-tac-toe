from .play_adapter import PlayAdapter
from .game import Game
from agent.agent import Agent
import numpy as np

class AgentAdapter(PlayAdapter):
    def __init__(self, agent: Agent, trainer=None, max_games=10, name=None):
        self.agent = agent
        self.game = None # is set by the play class right now so we can have multiple games
        self.trainer = trainer
        self.max_games = max_games
        self.game_count = 0
        self.name = name if name else agent.name

    def get_name(self) -> str:
        return self.name

    def next_move(self) -> str:
        if(self.game.active_player == self.agent.name):
            state = self._convert(self.game.board)
            action = self.agent.predict(state)
            return self._index_to_pos(action)
        
    def new_game(self) -> bool:
        self.game_count += 1
        return self.game_count < self.max_games
    
    def set_game(self, game):
        self.game = game

    def _convert(self, board):
        # convert the board to a (x,x) Tensor with values 0.0 = empty, 1.0 = agent, -1.0 = opponent
        state = np.zeros(self.game.board.size**2)
        symbol = "X" if self.game.players[0] == self.agent.name else "O"
        
        for x, y in board:
            value = 0.0 if board[(x, y)] == "" else 1.0 if board[(x, y)] == symbol else -1.0
            state[self._pos_to_index((x, y))] = value
        return state
    
    def _pos_to_index(self, pos):
        x, y = pos
        return x*self.game.board.size + y
    
    def _index_to_pos(self, index):
        return (index // self.game.board.size, index % self.game.board.size)