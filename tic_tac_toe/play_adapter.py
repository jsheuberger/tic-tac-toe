from typing import Any, Tuple
import abc

class PlayAdapter(abc.ABC):

    @abc.abstractmethod
    def next_move(self) -> Tuple[int, int]:
        pass

    @abc.abstractmethod
    def get_name(self) -> str:
        pass

    @abc.abstractmethod
    def new_game(self):
        pass

    @abc.abstractmethod
    def set_game(self, game):
        pass

