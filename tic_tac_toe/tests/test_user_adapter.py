from unittest import TestCase
from unittest.mock import patch
from ..user_adapter import UserAdapter
from ..ui import UI

class TestUserAdapter(TestCase):

    @patch("builtins.input", return_value="test123")
    def test_get_name(self, mock_input):
        self.assertEqual("test123", UserAdapter(UI()).get_name())

    @patch("builtins.input", return_value="1,2")
    def test_next_move(self, mock_input):
        self.assertEqual((1, 2), UserAdapter(UI()).next_move())
    
    @patch("builtins.input", return_value="Y")
    def test_new_game(self, mock_input):
        self.assertTrue(UserAdapter(UI()).new_game())

    @patch("builtins.input", return_value="quit")
    def test_next_move_quit(self, mock_input):
        self.assertEqual(None, UserAdapter(UI()).next_move())
    
    @patch("builtins.input", side_effect=["test", "2,", "quit", ""])
    def test_next_move_illegal_then_quit(self, mock_input):
        self.assertEqual(None, UserAdapter(UI()).next_move())
        self.assertEqual(3, mock_input.call_count)
        