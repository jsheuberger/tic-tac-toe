class Board(object):
    def __init__(self, size=3):
        self.size = size
        self.board = {(x, y): "" for x in range(size) for y in range(size)}

    def __call__(self):
        self.board = []
    
    def __str__(self):
        return f"board of size: {self.size}"

    def __repr__(self):
        """
        Generate a string representation of the Tic Tac Toe board.

        Returns:
            str: The string representation of the Tic Tac Toe board.
        """
        output = ""
        output += "-" * ((self.size * 2) + 1) + "\n"
        for row in range(self.size):
            output += "|"
            for column in range(self.size):
                cell = self.board[(column, row)]
                if cell == "":
                    output += " "
                else:
                    output += cell
                if column != 0: output += ""
                output += "|"
            output += "\n"
            output += "-" * ((self.size * 2) + 1)
            output += "\n"
        return output
    
    def __setitem__(self, position, symbol):
        """
        Set the value of a cell on the game board.

        Parameters:
            position (tuple): The position of the cell on the board. The tuple should contain the row and column indices.
            symbol (str): The symbol to be set on the cell. It can be either "X" or "O".

        Raises:
            ValueError: If the symbol is neither "X" nor "O".
            ValueError: If the position is out of bounds.
            ValueError: If the position is already occupied.

        Returns:
            None
        """
        # Check if symbol is X or O
        if symbol != "X" and symbol != "O":
            raise ValueError("Symbol must be X or O")
        # Check if position is valid
        if position[0] < 0 or position[0] >= self.size or position[1] < 0 or position[1] >= self.size:
            raise ValueError("Position is out of bounds")
        # Check if position is already occupied
        if self.board[position] != "":
            raise ValueError("Position is already occupied")
        self.board[position] = symbol
    
    def __getitem__(self, position):
        """
        Retrieve an item from the board at the given position.

        Parameters:
            position (int): The index of the item to retrieve.

        Returns:
            Any: The item at the given position in the board.
        """
        return self.board[position]

    def __iter__(self):
        """
        Enumerate all the (x,y) tuples on the board.

        Returns:
            list: A list of all the (x,y) tuples on the board.
        """
        return iter([(x, y) for x in range(self.size) for y in range(self.size)])
    
    def three_in_a_row(self, symbol):
        """
        Check if there are three consecutive cells in a row, column, or diagonal that have the specified symbol.

        Parameters:
            symbol (str): The symbol being checked.

        Returns:
            bool: True if there are three consecutive cells with the symbol, False otherwise.
        """
        # Check rows
        if any(all(self.board[(row, col)] == symbol for col in range(self.size)) for row in range(self.size)):
            return True

        # Check columns
        if any(all(self.board[(row, col)] == symbol for row in range(self.size)) for col in range(self.size)):
            return True

        # Check main diagonal
        if all(self.board[(i, i)] == symbol for i in range(self.size)):
            return True

        # Check secondary diagonal
        if all(self.board[(i, self.size - 1 - i)] == symbol for i in range(self.size)):
            return True

        return False
