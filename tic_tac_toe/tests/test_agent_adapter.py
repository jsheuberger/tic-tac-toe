from unittest import TestCase
from unittest.mock import MagicMock
from ..agent_adapter import AgentAdapter
from agent import Agent
from tic_tac_toe import Game, Board

class TestAgentAdapter(TestCase):
    
    def setUp(self):
        self.m_agent = MagicMock(spec=Agent)
        self.m_game = MagicMock(spec=Game)

    def test_get_name(self):
        self.m_agent.name = "mockAgent"
        self.m_agent.predict.return_value = 0

        adapter = AgentAdapter(self.m_agent, self.m_game, max_games=1)
        self.assertEqual("mockAgent",adapter.get_name())

    def test_convert(self):
        board = Board(3)
        board.board = {(0, 0): "X", (0, 1): "", (0, 2): "", (1,0): "X", (1, 1): "O", (1, 2): "", (2, 0): "O", (2, 1): "", (2, 2): ""}
        self.m_game.board = board
        self.m_game.players = ["test", "_"]
        self.m_agent.name = "test"

        adapter = AgentAdapter(self.m_agent, self.m_game, max_games=1)
        state = adapter._convert(self.m_game.board)
        self.assertListEqual([1.0, 0.0, 0.0, 1.0, -1.0, 0.0, -1.0, 0.0, 0.0], state.tolist())

    def test_index_to_pos(self):
        board = Board(3)
        self.m_game.board = board
        
        adapter = AgentAdapter(self.m_agent, self.m_game, max_games=1)
        self.assertEqual((0, 0), adapter._index_to_pos(0))
        self.assertEqual((0, 1), adapter._index_to_pos(1))
        self.assertEqual((0, 2), adapter._index_to_pos(2))
        self.assertEqual((1, 0), adapter._index_to_pos(3))
        self.assertEqual((1, 1), adapter._index_to_pos(4))
        self.assertEqual((1, 2), adapter._index_to_pos(5))
        self.assertEqual((2, 0), adapter._index_to_pos(6))
        self.assertEqual((2, 1), adapter._index_to_pos(7))
        self.assertEqual((2, 2), adapter._index_to_pos(8))

    def test_pos_to_index(self):
        board = Board(3)
        self.m_game.board = board
        
        adapter = AgentAdapter(self.m_agent, self.m_game, max_games=1)
        self.assertEqual(0, adapter._pos_to_index((0, 0)))
        self.assertEqual(1, adapter._pos_to_index((0, 1)))
        self.assertEqual(2, adapter._pos_to_index((0, 2)))
        self.assertEqual(3, adapter._pos_to_index((1, 0)))
        self.assertEqual(4, adapter._pos_to_index((1, 1)))
        self.assertEqual(5, adapter._pos_to_index((1, 2)))
        self.assertEqual(6, adapter._pos_to_index((2, 0)))
        self.assertEqual(7, adapter._pos_to_index((2, 1)))
        self.assertEqual(8, adapter._pos_to_index((2, 2)))