import time
from tic_tac_toe.ui import UI
from tic_tac_toe.game import Game

class Replay():

    def __init__(self, ui):
        self.ui = ui

    def replay(self, records, players=None, delay=1000):
        self.ui.message("Replay of the game")

        if(players is None):
            players = self._get_players(records)
        p1,p2 = players
        game = Game(p1,p2)
        if(game.active_player is None):
            if len(records) > 0:
                game(records[0][0])
            else:
                game(p1)
        
        self.ui.display(game)
        if(delay > 0):
            time.sleep(delay/1000)
        
        for player, move in records:
            game.play(player, move)
            self.ui.display(game)
            if(delay > 0):
                time.sleep(delay/1000)
            else:
                self.ui.prompt("Press enter to continue")
            
            if(game.winner is not None):
                self.ui.message(f"Winner: {game.winner}")
                break

    def _get_players(self, records):
        print(len(records))
        if(len(records) >= 2):
            p1 = records[0][0]
            p2 = records[1][0]
            return p1, p2
        raise Exception("not enough records")
    
def load_from_json(json):
    players = tuple(json["players"])
    games = json["games"]

    all_moves = []
    for game in games:
        all_moves.append([(move["player"], tuple(map(int, move["move"].split(",")))) for move in game])
    return (players, all_moves)
