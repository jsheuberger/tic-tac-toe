from typing import Any
import blessings

class UI:

    def __init__(self):
        self.terminal = blessings.Terminal()

    def __call__(self):
        print(self.terminal.fullscreen())
        print(self.terminal.clear())

    def close(self):
        print(self.terminal.exit_full_screen())

    def message(self, msg):
        with self.terminal.location(0, 16):
            print(msg)

    def display(self, game):
        print(self.terminal.clear())
        with self.terminal.location(0,0):
            game.print()

    def prompt(self, msg):
        with self.terminal.location(0, 17):
            return input(msg)