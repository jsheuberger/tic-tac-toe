import random
from .board import Board

class Game(object):
    """
    A class representing a game of tic-tac-toe.

    Attributes:
        players (list): A list of two players.
        active_player (str): Name of the active player.
        board (Board): The game board.
        winner (Player): The winner of the game.

    """

    def __init__(self, player1: str, player2: str, size=3, record=False):
        """
        Initializes a new instance of the Game class.

        Parameters:
            player1 (Player): The first player.
            player2 (Player): The second player.
            size (int): The size of the game board (default 3).
            record (bool): Whether to record the game history (default False).

        Returns:
            None
        """
        self.active_player = None
        self.players = [player1, player2]
        self.board = Board(size)
        self.winner = None
        self.is_recorded = record
        self.record = []

    def __call__(self, active_player=None):
        """
        Initializes the active player for the game.

        Parameters:
            active_player (Optional[str]): The name of the active player. Defaults to None.

        Returns:
            None
        """
        self.active_player = active_player if active_player else self.players[random.randint(0, 1)]

    def print(self):
        """
        Print the current game state.

        This function prints the current game state, including the players, the winner (if there is one),
        the active player, and the game board. It does not return anything.

        Parameters:
            self (object): The instance of the game class.

        Returns:
            None
        """
        print(f"X: {self.players[0]}")
        print(f"O: {self.players[1]}")
        if self.winner: print(f"winner: {self.winner}")
        else: print(f"active player: {self.active_player}")
        print(repr(self.board))
    
    def play(self, player, position):
        """
        Plays a move in the game.

        Parameters:
            player (str): The player making the move.
            position (int): The position on the board where the move is being made.

        Raises:
            Exception: If the game is already over.
            Exception: If the wrong player is trying to make a move.
            Exception: If the move is illegal.

        Returns:
            None
        """
        if self.winner: raise Exception("game over")
        if self.active_player != player: raise Exception("wrong player")

        if self.is_recorded: self.record.append((player, position))

        if self.board[position] != "": raise Exception("illegal move")

        self.board[position] = self._symbol(player)
        if self.board.three_in_a_row(self._symbol(self.active_player)): 
            self.winner = self.active_player
            return # Done

        self._next_player()

        if not self._is_move_available() and not self.winner:
            self.winner = "Draw"
    
    def available_moves(self):
        """
        Generate a list of available moves on the game board.

        Returns:
            A list of tuples representing the coordinates of empty cells on the board.
        """
        moves = []
        for x, y in self.board:
            if self.board[(x, y)] == "":
                moves.append((x, y))
        return moves

    # Internal methods

    def _next_player(self):
        # Make the next player active
        self.active_player = self.players[1] if self.active_player == self.players[0] else self.players[0]

    def _symbol(self, player):
        # Convert a name to a symbol on the board
        return "X" if player == self.players[0] else "O"
    
    def _is_move_available(self):
        # Test if there are any moves available
        for(x, y) in self.board:
            if self.board[(x, y)] == "":
                # Fast succeed
                return True 
        return False