import tensorflow_probability as tfp
import tensorflow as tf
import numpy as np

@tf.keras.utils.register_keras_serializable()
class PolicyGradientModel(tf.keras.Model):
    
    def __init__(self, state_size, action_size, hidden_size: tuple):
        super().__init__()
        self.state_size = state_size
        self.action_size = action_size
        self.model = tf.keras.Sequential()
        for dims in hidden_size:
            self.model.add(tf.keras.layers.Dense(dims, activation='relu'))
        self.model.add(tf.keras.layers.Dense(action_size, activation='sigmoid'))

    def call(self, inputs, training=False) -> tf.Tensor:
        """
        Calls the policy model with the specified inputs.

        Args:
            inputs (tf.Tensor): The input tensor.

        Returns:
            Tensor: The output tensor of action logits.
        """
        state = np.expand_dims(inputs, axis=0)
        x = self.model(state, training=training)
        return x
    
    def sample(self, logits) -> int:
        """
        Calls the policy model with the predicted inputs, and returns a random sampled action.

        Args:
            inputs (tf.Tensor): The inputs tensor.

        Returns:
            Tensor: The action that was sampled from the policy logits.
        """
        # Convert the logits to probabilities using softmax
        probs = tf.nn.softmax(logits)
        dist = tfp.distributions.Categorical(probs=probs, dtype=tf.int32)
        sample = dist.sample()
        return tf.squeeze(sample, axis=0).numpy()
    
    def save(self, filename):
        self.model.save(filename)

    def load(self, filename):
        self.model = tf.keras.models.load_model(filename)
        self.model.summary()