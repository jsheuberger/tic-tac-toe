class Rewards(object):

    def reward(self, game, name):
        if game.winner == None:
            return 0.0
        elif game.winner == "Draw": #draws
            return 0.25
        elif game.winner == name: # wins
            return 1.0
        else:
            return -1.0 # losses
    
    def exception_reward(self, _exception):
        return -1.0