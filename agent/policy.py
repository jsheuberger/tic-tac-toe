import abc

class Policy(abc.ABC):
    
    @abc.abstractmethod
    def sample(self, input):
        pass