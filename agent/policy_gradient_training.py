import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from tic_tac_toe.game import Game


class PolicyGradientTraining(object):

    def __init__(self, reward_object=None, learning_rate=0.001, gamma=1., epsilon=1e-9):
        self.reward_object = reward_object
        self.optimizer = tf.optimizers.Adam(learning_rate=learning_rate)
        self.gamma = gamma
        self.epsilon = epsilon

    def train(self, model, episode_count=100, batch_size=10, display=False, self_play=False):

        model.optimizer = self.optimizer
        
        illegal_move_cnt = 0
        win_cnt = [0,0,0]

        # we are training 2 bots simultaneously
        players = ['training-1', 'training-2']
        
        batch = []
        metrics = {'results':[], 'illegal_moves':[]}
        results = {'win': 0, 'draw': 0, 'loss': 0}
            
        for episode in range(episode_count):
            # play a game
            states = {}
            actions = {}
            logits = {}
            rewards = {}

            for player in players:
                states[player] = []
                actions[player] = []
                logits[player] = []
                rewards[player] = []

            game = Game(player1=players[0], player2=players[1])
            game()
            if(display):
                game.print()

            while(game.winner == None):
                player = game.active_player

                state = self._convert(game, player)
                available_moves = [self._pos_to_index(game.board.size, pos) for pos in game.available_moves()]
                # Create an array with values 1.0 if the index is in available moves, and 0.0 if it isn't
                mask = np.array([1. if index in available_moves else 0.0 for index in range(game.board.size**2)])

                if(player == players[0] or self_play):
                    outputs = model(state)
                    probs = tf.nn.softmax(outputs)
                    # filter out invalid moves by setting probability to 0. This works using the Categorical distribution probs
                    # Probably not the cleanest way to go about this though
                    dist = tfp.distributions.Categorical(probs=probs*mask, dtype=tf.int32)
                    action = tf.squeeze(dist.sample(1)).numpy()
                else: # train against random moves
                    action = available_moves[np.random.randint(len(available_moves))]
                    # print(f"random move: {self._index_to_pos(game.board.size, action)}")
                
                move = self._index_to_pos(game.board.size, action)
                
				# store the data for reference
                states[player].append(state)
                actions[player].append(action)
                try:
                    game.play(player, move)

                    if(display): 
                        game.print()
                    # record any direct rewards
                    rewards[player].append(self.reward_object.reward(game, player))
                except Exception as e:
                    # or exceptional rewards
                    rewards[player].append(self.reward_object.exception_reward(e))
                    # just try another move until we succeed
                    if(player == players[0]):
                        illegal_move_cnt += 1

            # update the last reward for the player who didn't win
            loser = players[1] if game.winner == players[0] else players[0]
            rewards[loser][-1] += self.reward_object.reward(game, loser)

            # Always from the perspective of player 1
            if(game.winner == players[0]):
                results['win'] += 1
            elif(game.winner == players[1]):
                results['loss'] += 1
            else:
                results['draw'] += 1

            active_players = players if self_play else [players[0]]
            for p in active_players:
                # when we are done, we can calculate the total rewards for each sampled action
                discounted_rewards = self.discount(rewards[p])

                # Create placeholder variables
                game_states = np.array(states[p])
                game_actions = np.array(actions[p])
                game_rewards = discounted_rewards

                batch.append((game_states, game_actions, game_rewards))

            if((episode+1) % batch_size == 0):
                all_states = []
                all_actions = []
                all_rewards = []
                for(states, actions, rewards) in batch:
                    all_states.extend(states)
                    all_actions.extend(actions)
                    all_rewards.extend(rewards)
                all_states = np.array(all_states)
                all_actions = np.array(all_actions)
                all_rewards = np.array(all_rewards)

                # normalize the rewards over all items in the batch
                all_rewards -= np.mean(all_rewards)
                all_rewards /= np.std(all_rewards)+self.epsilon

                batch_loss = self._train_batch(model, all_states, all_actions, all_rewards)
                
                batch_cnt = int(episode / batch_size)
                print(f"batch {batch_cnt} avg loss: {np.mean(batch_loss)}, std: {np.std(batch_loss)}")
                print(f"wins: {results['win']}, draws: {results['draw']}, losses: {results['loss']}")
                print(f"illegal moves: {illegal_move_cnt}")
                batch = []
                metrics['results'].append(results)
                results = {'win': 0, 'draw': 0, 'loss': 0}
                metrics['illegal_moves'].append(illegal_move_cnt)
                illegal_move_cnt = 0

        total_results = {'win': 0, 'draw': 0, 'loss': 0}

        for result in metrics['results']:
            total_results['win'] += result['win']
            total_results['draw'] += result['draw']
            total_results['loss'] += result['loss']

        print()
        print(f"trained for {episode_count} games")
        print(f"wins: {total_results['win']}, draws: {total_results['draw']}, losses: {total_results['loss']}")
        print(f"illegal moves: {sum(metrics['illegal_moves'])}")

    def discount(self, rewards):
        discounted_rewards = []
        cumulative_reward = 0
        for reward in rewards[::-1]:
            cumulative_reward = reward + cumulative_reward * self.gamma
            discounted_rewards.append(cumulative_reward)
        
        discounted_rewards.reverse()
        return np.array(discounted_rewards)

    def _train_batch(self, model, states, actions, rewards):
        # update the policy
        m = model.model
        with tf.GradientTape() as tape:
            outputs = m(states, training=True)
            probs = tf.nn.softmax(outputs)
            dist = tfp.distributions.Categorical(probs=probs, dtype=tf.int32)
            logps = dist.log_prob(actions)
            losses = -logps*rewards

        # print(probs, rewards, losses)
        # update the policy
        gradients = tape.gradient(losses, m.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        return losses

    def _convert(self, game, name):
        # convert the board to a (x,x) Tensor with values 0.0 = empty, 1.0 = agent, -1.0 = opponent
        state = np.zeros(game.board.size**2)
        symbol = "X" if game.players[0] == name else "O"
        
        for x, y in game.board:
            value = 0.0 if game.board[(x, y)] == "" else 1.0 if game.board[(x, y)] == symbol else -1.0
            state[self._pos_to_index(game.board.size, (x, y))] = value
        return state
    
    def _pos_to_index(self, size, pos):
        x, y = pos
        return x*size + y
    
    def _index_to_pos(self, size, index):
        return (index // size, index % size)