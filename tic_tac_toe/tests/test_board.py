import unittest
from tic_tac_toe.game import Board

class TestBoard(unittest.TestCase):
    def test_init(self):
        board = Board(3)
        self.assertEqual(board.size, 3)
        self.assertEqual(board.board, {(0, 0): "", (0, 1): "", (0, 2): "",
                                       (1, 0): "", (1, 1): "", (1, 2): "",
                                       (2, 0): "", (2, 1): "", (2, 2): ""})

    def test_repr(self):
        board = Board(3)
        self.assertEqual(repr(board), 3*"-------\n| | | |\n"+"-------\n")

    def test_str(self):
        board = Board(3)
        self.assertEqual(str(board), "board of size: 3")
    
    def test_three_in_a_row_false(self):
        board = Board(3)
        self.assertEqual(board.three_in_a_row("X"), False)

    def test_three_in_a_row_on_column(self):
        board = Board(3)
        board[(0, 0)] = "X"
        board[(0, 1)] = "X"
        board[(0, 2)] = "X"
        self.assertEqual(board.three_in_a_row("X"), True)

    def test_three_in_a_row_on_row(self):
        board = Board(3)
        board[(0, 0)] = "X"
        board[(1, 0)] = "X"
        board[(2, 0)] = "X"
        self.assertEqual(board.three_in_a_row("X"), True)

    def test_three_in_a_row_on_diagonal(self):
        board = Board(3)
        board[(0, 0)] = "X"
        board[(1, 1)] = "X"
        board[(2, 2)] = "X"
        self.assertEqual(board.three_in_a_row("X"), True)

    def test_three_in_a_row_on_column_false(self):
        board = Board(3)
        board[(0, 0)] = "X"
        board[(0, 1)] = "O"
        board[(0, 2)] = "X"
        self.assertEqual(board.three_in_a_row("X"), False)