# Agent

The self learning agent will learn to play tic-tac-toe by playing multiple games against another agent.

## Design

input: the model state
output: probability of the next action to take
reward:
- win game: 1.0 points
- lose game: -1.0 points
- draw game: 0.25 point
- illegal move: -1.0 points

## Implementation details

The Policy function π(s,a) function is defined as the probability of taking a given action a when in a state s when trying to maximize the expected total reward.

```π(s,a) = P(s,a)```

Optimizing this function should lead us to the best policy for our agent. At every state, it should be able to determine the best action to take.

### Policy Gradient

We play games of tic-tac-toe until we either win or lose. Then we calculate the rewards for every action we took, and backpropagate these rewards after normalizing them.

The policy generates a probability distribution over the possible actions, leading to taking one of the possible actions. We finish playing the game, calculate the total reward for every action we took based on the final result, and the direct reward after the action. We update the policy network using the reward as if it was the actual probability of the taken action.

If the action tensor was [a1, a2, a3, a4, ...] and resulted in the probabilities [0.12, 0.07, 0.8, 0.5, ...]. And the selected action was a3. If the game ended in a positive reward rt = 6.3, we would plug in the reward at position of the action taken (a3) = [ 0.0, 0.0, 6.3, 0.0, ...]. We would do the same for all actions taken.

#### Progress

- [x] Training of models using a single player per episode training approach is working. 
- [ ] Training in batches of X episodes per set
- [ ] Training in games vs self
- [ ] Multi-threaded training

#### Behavior

- After 5k games vs Random policy -> Able to close a row, unable to prevent player from creating a row, selecting sub optimal first positions
- After 20k games vs Random policy

### TODO Q learning

The Value function V(s) function is defined as the expected total reward of a state s. Thus this describes the value of being in state s at any point in time.

```v(s) = E(Σ r(s,a)*P(s,a))```

Since the problem in question has a very simple and defined state, we can use basic Reinforcement learning techniques to optimize the policy function π().

We create a Deep Policy Network where we update the parameters using backpropagation based on the expected reward of taking an action a in state s.

We create a Value Network and a Policy Network where the value network tries to optimize the value of our expected state s' and the policy network tries to optimize for the best policy to use we are in any given state s.


